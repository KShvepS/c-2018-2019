﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Sudoku
{
    /// <summary>
    /// Логика взаимодействия для Win.xaml
    /// </summary>
    public partial class Win : Window
    {
        public Win(string textWin)
        {
            InitializeComponent();
            winTime.Content = "Длительность игры составляет " + textWin;
        }
        //закрыть окно победы
        private void ClickOk(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
