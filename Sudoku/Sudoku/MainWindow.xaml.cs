﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Sudoku
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public const int SIZE = 9;//размер поля
        SudokuField gameField;
        public System.Windows.Threading.DispatcherTimer dispatcherTimer;//создаём таймер
        public int hours, seconds, minutes;// для таймера
        public TextBox helpTextBoxNow; // текущий текстбокс, к которой мы должны подобрать 
        // числа в подсказке

        public MainWindow()
        {
            InitializeComponent();
            levelOfDif.SelectedIndex = 0;// уровень "легко по умолчанию"
            startGame();// Запускаем игру
            dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);// устанавливаем интервал
            dispatcherTimer.Start(); // запускаем таймер
        }
        /// <summary>
        /// Метод для запуска новой игры
        /// </summary>
        private void startGame()
        {
            seconds = minutes = hours = 0; // обнуляем значения
            gameField = new SudokuField(levelOfDif.SelectedIndex);
            TextBox txtBox;//заполняемый текстбокс
            string textBoxName = "";//имя текстбокса
            for (int i = 0; i < SIZE; i++)// проходимся по всем текстбоксам
            {
                for (int j = 0; j < SIZE; j++)
                {
                    textBoxName = "_" + i.ToString() + "_" + j.ToString();
                    txtBox = (TextBox)FindName(textBoxName);
                    txtBox.Text = gameField.field[i, j].ToString();
                    txtBox.Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 0));
                    txtBox.IsReadOnly = true; // запрещаем ввод в ячейки, где есть цифры
                }
                clearSteps(); //очищаем поле от "выколотых" чисел
            }
        }
        /// <summary>
        /// Метод для стирания выколотых чисел на поле
        /// </summary>
        private void clearSteps()
        {
            seconds = minutes = hours = 0; // обнуляем значения
            TextBox txtBox;//заполняемый текстбокс
            string textBoxName = "";//имя текстбокса
            // очищаем те текстбоксов, что "выколоты"
            for (int i = 0; i < gameField.puncturedNumbers.Length / 2; i++)
            {
                textBoxName = "_" + gameField.puncturedNumbers[i, 0].ToString() + "_"
                    + gameField.puncturedNumbers[i, 1].ToString();
                txtBox = (TextBox)FindName(textBoxName);
                txtBox.Text = "";
                txtBox.KeyUp += enterDigit;
                txtBox.PreviewTextInput += InputText;
                txtBox.TextChanged += feelDigit;
                txtBox.GotKeyboardFocus += Focus;
            }
            checkReadOnly(false); // вызываем метод, который сделает доступными для ввода
            // выколотые ячейки
        }
        /// <summary>
        /// при нажатии кнопки "Новая игра"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void createNewGame(object sender, RoutedEventArgs e)
        {
            startGame();
        }
         /// <summary>
         /// При нажатии на текстбокс проверяем на победу
         /// </summary>
         /// <param name="sender"></param>
         /// <param name="e"></param>
        private void enterDigit(object sender, KeyEventArgs e)
        {
            checkWin();
        }

        /// <summary>
        /// Проверка на победу
        /// </summary>
        private void checkWin()
        {
            bool iswin = true;
            TextBox txtBox; 
            for(int i = 0; i < gameField.puncturedNumbers.Length/2; i++)
            {
                string txtBoxName = "_" + gameField.puncturedNumbers[i, 0].ToString() + "_"
                    + gameField.puncturedNumbers[i, 1].ToString();
                txtBox = (TextBox)FindName(txtBoxName);
                if (!txtBox.Text.Equals(gameField.field[(gameField.puncturedNumbers[i, 0]), (gameField.puncturedNumbers[i, 1])].ToString()))
                {
                    iswin = false;
                }
            }

            if(iswin == true)//если победили- открываем окно победы
            {
                Win winWindow = new Win(time.Content.ToString());
                Hide();
                winWindow.ShowDialog();
                Show();
                startGame();// Запускаем игру заново
            }
        }
        /// <summary>
        /// Метод, который разрешает вводить только однозначеные десят числа
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InputText(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !char.IsDigit(Convert.ToChar(e.Text)) || 
            (sender as TextBox).Text.Length==1;
        }
        /// <summary>
        /// Метод для нажатия кнопки "Пауза" и "Продолжить"  СДЕЛАТЬ НЕДОСТУПНОСТЬ КНОПОК
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void stopContinueGame(object sender, RoutedEventArgs e)
        {
            //Меняем значение текста кнопки по нажатию
            if((sender as Button).Content.Equals("Пауза"))
            {
                (sender as Button).Content = "Продолжить";
                dispatcherTimer.Stop();
                checkReadOnly(true);
            }
            else
            {
                (sender as Button).Content = "Пауза";
                dispatcherTimer.Start();
                checkReadOnly(false);
            }
            
        }
        private void checkReadOnly(bool readOnly)
        {
            TextBox txtBox;//заполняемый текстбокс
            string textBoxName = "";//имя текстбокса
            if (readOnly)
            {
                for (int i = 0; i < gameField.puncturedNumbers.Length / 2; i++)
                {
                    textBoxName = "_" + gameField.puncturedNumbers[i, 0].ToString() + "_"
                        + gameField.puncturedNumbers[i, 1].ToString();
                    txtBox = (TextBox)FindName(textBoxName);
                    txtBox.IsReadOnly = true; // запрещаем ввод в ячейки
                }
            }
            else
            {
                for (int i = 0; i < gameField.puncturedNumbers.Length / 2; i++)
                {
                    textBoxName = "_" + gameField.puncturedNumbers[i, 0].ToString() + "_"
                        + gameField.puncturedNumbers[i, 1].ToString();
                    txtBox = (TextBox)FindName(textBoxName);
                    txtBox.IsReadOnly = false; // реазрешаем ввод в ячейки
                }
            }
        }

        /// <summary>
        /// Метод для 
        /// Расскрашивания записанной цифры
        /// </summary>
        private void drawColorDigit()
        {
            //выбираем координаты текстбокса
            int x = Convert.ToInt16(helpTextBoxNow.Name[1].ToString());
            int y = Convert.ToInt16(helpTextBoxNow.Name[3].ToString());
            TextBox tempTextBox;
            string nameTextBox;
            bool haveDigitX, haveDigitY;//есть ли уже указанное число
            haveDigitX = haveDigitY = false;
            //проходим по строке
            for (int i = 0; i < SIZE; i++)
            {
                if(i != y) // если не наш текстбокс
                {
                    //проходим по текстбоксам из строки
                    nameTextBox = "_" + x.ToString() + "_" + i.ToString();
                    tempTextBox = (TextBox)FindName(nameTextBox);
                    if (tempTextBox.Text.Equals(helpTextBoxNow.Text))//если есть значение уже
                    {
                        haveDigitY = true;
                    }
                }
            }
            //проходим по столбцу
            for (int i = 0; i < SIZE; i++)
            {
                if (i != x) // если не наш текстбокс
                {
                    //проходим по текстбоксам из строки
                    nameTextBox = "_" + i.ToString() + "_" + y.ToString();
                    tempTextBox = (TextBox)FindName(nameTextBox);
                    if (tempTextBox.Text.Equals(helpTextBoxNow.Text))//если есть значение уже
                    {
                        haveDigitX = true;
                    }
                }
            }
            // если по строкам или столбцам уже есть такие числа
            if(haveDigitX || haveDigitY)
            {
                //красим в красный
                helpTextBoxNow.Foreground = new SolidColorBrush(Color.FromRgb(255,0,0));
            }
            else// возвращаем нормальный цвет
            {
                helpTextBoxNow.Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 0));
            }
        }
        /// <summary>
        /// Метод при нажатии на вспомогательные кнопки заполнения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void getHelp(object sender, RoutedEventArgs e)
        {
            // заполняем значение текстбокса
            if(!(sender as Button).Content.Equals("Очистить"))
            {
                helpTextBoxNow.Text = (sender as Button).Content.ToString();
            }
            else
            {
                helpTextBoxNow.Text = "";
            }
            checkWin();//проверяем на победу
        }

        /// <summary>
        /// Метод для ручного запонения ячейки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
       private void feelDigit(object sender, TextChangedEventArgs e)
        {
            helpTextBoxNow = (sender as TextBox);
            drawColorDigit();//раскрашиваем цифру
        }
        /// <summary>
        /// Метод для запоминания нажатого текстбокса для его заполнения с помощью 
        /// вспомогательных кнопок 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Focus(object sender, KeyboardFocusChangedEventArgs e)
        {
            helpTextBoxNow = (sender as TextBox);
        }

        /// <summary>
        /// При нажатии на кнопку "Сбросить" очищаем заполненные числа
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void resetField(object sender, RoutedEventArgs e)
        {
            clearSteps();
        }
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            // Пишем значение времени в лэйбл
            seconds += 1;
            if(seconds == 60)
            {
                minutes += 1;
                seconds = 0;
            }
            if(minutes == 60)
            {
                hours += 1;
                minutes = 0;
            }
            string secondsString, minutesString, hoursString;// для записи времени
            secondsString = seconds.ToString();
            if (seconds/10==0)
            {
                secondsString = "0" + secondsString;
            }
            minutesString = minutes.ToString();
            if (minutes / 10 == 0)
            {
                minutesString = "0" + minutesString;
            }
            hoursString = hours.ToString();
            if (hours / 10 == 0)
            {
                hoursString = "0" + hoursString;
            }

            time.Content = hoursString + ":"+ minutesString  + ":" + secondsString;
            // Вызов события
            CommandManager.InvalidateRequerySuggested();
        }
    }
}
