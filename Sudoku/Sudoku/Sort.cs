﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sudoku
{

    class Sort
    {
        private const int SIZE = 9;
        int[,] t; // дополнительный массив для перемешивания и изменения значений основного, посылаемого в методы

        /// <summary>
        /// перемешивания столбцов и строк массива
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        public int[,] sortXY(int[,] field)
        {
            t = new int[SIZE, SIZE];
            for (int i = 0; i < SIZE; i++)
            {
                for (int j = 0; j < SIZE; j++)
                {
                    t[i, j] = field[j, i];
                }
            }
            return t;
        }
        /// <summary>
        /// перемешивания столбцов или строк массива
        /// </summary>
        /// <param name="field"></param>
        /// <param name="firstValue"></param>
        /// <param name="secondValue"></param>
        /// <param name="task"></param>
        /// <returns></returns>
        public int[,] changeColumnsOrRows(int[,] field, int firstValue, int secondValue, string task)
        {
            int tempElement; //временный элемент для смены значений
            t = field;
            if (task.Equals("column")) 
            {
                for (int i = 0; i < SIZE; i++)
                {
                    tempElement = t[i,firstValue];
                    t[i, firstValue] = t[i, secondValue];
                    t[i, secondValue] = tempElement;
                }
            }
            else /// task.Equals("row");
            {
                for (int i = 0; i < SIZE; i++)
                {
                    tempElement = t[firstValue, i];
                    t[firstValue, i] = t[secondValue, i];
                    t[secondValue, i] = tempElement;
                }
            }

            return t;
        }

        /// <summary>
        /// Метод для обмена местами районов (по строкам и по столбцам)
        /// </summary>
        /// <param name="field"></param>
        /// <param name="firstValue"></param>
        /// <param name="secondValue"></param>
        /// <param name="task"></param>
        /// <returns></returns>
        public int[,] changeDistrict(int[,] field, int firstValue, int secondValue, string task)
        {
            t = field;
            for (int i = 0; i < 3; i++)
            {
                t = changeColumnsOrRows(t, firstValue+i, secondValue+i, task);
            }

            return t;
        }
/*
        private int[] changeRowOfDigits(int [] rowOfDigits, int firstValue, int secondValue)
        {
            int[] temp = new int[SIZE];

            temp = rowOfDigits;
            Array.Copy(rowOfDigits, firstValue, temp, secondValue, 3);
            Array.Copy(rowOfDigits, secondValue, temp, firstValue, 3);

            return temp; 
        }*/
    }
}
