﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sudoku
{
    class SudokuField
    {
        public int[,] field; // главное поле
        private int[] digits; // вспомогателный массив
        public int[,] puncturedNumbers;//координаты выколотых чисел
        public const int SIZE = 9;//размер поля
        private Random rand = new Random(); // рандом для всего
        public Sort newSort = new Sort(); // для сортировки поля

        /// <summary>
        /// Rонструктор класса
        /// </summary>
        /// <param name="level"></param>
        public SudokuField(int level)
        {
            field = new int[SIZE, SIZE];
            puncturedNumbers = new int[(level+1)*SIZE, 2]; // инициализируем массив индексов выколотых чисел
            fillField();// заполняем поле
            sortField();// сортируем поле
            pickOutNumbers(level);
        }

        /// <summary>
        /// Метод заполнения поля, когда весь судоку отсортирован
        /// </summary>
        private void fillField()
        {
            int firstRegionX, errorY;//смещение по х и у
            firstRegionX = errorY = 0;
            digits = new int[SIZE];

            for (int i = 0; i < SIZE; i++)
            {
                if (i == 3 || i == 6)
                {
                    errorY = 0;
                    firstRegionX += 1;
                }
                shakeDigits(firstRegionX, errorY);
                for (int j = 0; j < SIZE; j++)
                {
                    field[i, j] = digits[j];
                }
                errorY += 3;
            }
        }

        private void shakeDigits( int x, int y) // изменение порядка цифр в строках поля
        {
            int[] tempDigits = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };//числа для заполнения
            int firstPosition = x + y; // итоговая позиция
            
            Array.Copy(tempDigits, firstPosition, digits, 0, SIZE - firstPosition);
            Array.Copy(tempDigits, 0, digits, SIZE - firstPosition, SIZE - (SIZE - firstPosition));
        }

        /// <summary>
        /// Метод для перемешивания массива
        /// </summary>
        private void sortField()
        {
            int lastRand, randLimit, taskIndex; // для сортировки строк и столбцов
            int firstDistrict, secondDistrict; // для сортировки дистриктов 
            string[] task = {"column", "row"}; // для обоихвариантов сортировок
            lastRand = randLimit = taskIndex = 0;

            for (int i = 0; i < SIZE*SIZE; i++)
            {
                field = newSort.sortXY(field);
                while (lastRand == randLimit)//рандомим числа для выбора дистрикта, 
                // в котором будем перемешивать столбцы
                {
                    lastRand = randLimit;
                    randLimit = rand.Next(1, 3);
                    taskIndex = rand.Next(0, 2);//для выбора, мы сортирую столбец или строку
                }
                sortColumnOrRow(randLimit*3, task[taskIndex]);//определяем предел лимита, умножив выпавшее число на 3
                //(так как нам нужно 3, 6 или 9)
                /// Сортируем дистрикты постолбцам или строкам
                firstDistrict = secondDistrict = 0;
                while (firstDistrict == secondDistrict)//рандомим числа для выбора дистриктов, 
                // которые будем менять местами
                {
                    firstDistrict = rand.Next(0, 3);
                    secondDistrict = rand.Next(0, 3);
                    taskIndex = rand.Next(0, 2);//для выбора, мы сортирую столбец или строку
                }
                //множим на 3, так как нам нужно начало каждого дистрикта (а это 0, 3, 6)
                field = newSort.changeDistrict(field, firstDistrict*3, secondDistrict*3, task[taskIndex]);
            }
        }

        /// <summary>
        /// Метод для вызова метода из класса Sort, для перемешивания строки или стобца
        /// </summary>
        /// <param name="randLimit"></param>
        /// <param name="task"></param>
        private void sortColumnOrRow(int randLimit, string task)
        {
            int first, second;
            first = second = 0;

            while (first == second)
            {
                first = rand.Next(randLimit-3, randLimit);
                second = rand.Next(randLimit-3, randLimit);
            }
            field = newSort.changeColumnsOrRows(field, first, second, task);
        }

        /// <summary>
        /// Метод для набора чисел, которые должны быть "выколоты" на поле
        /// </summary>
        /// <param name="level"></param>
        private void pickOutNumbers(int level)
        {
            int x, y;
            bool equals = false;//для проверки была ли найдена такая же пара чисел
            int addedNumbers = 0; // количество добавленных в массив чисел
            while(addedNumbers < (level + 1) * SIZE)
            {
                x = rand.Next(0, SIZE);
                y = rand.Next(0, SIZE);
                for(int i = 0; i < addedNumbers; i++)
                {
                    //для отсутствия повторений в массиве
                    if(puncturedNumbers[i, 0] == x && puncturedNumbers[i, 1] == y)
                    {
                        equals = true;
                    }
                }
                if(equals == false)
                {
                    puncturedNumbers[addedNumbers, 0] = x;
                    puncturedNumbers[addedNumbers, 1] = y;
                    addedNumbers++;
                }
                equals = false;
            }
            
        }
    }
}
