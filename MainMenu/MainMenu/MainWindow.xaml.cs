﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Sudoku;
using Pyatnashki;
using Calculator;
using ProstoyCalculator;

namespace MainMenu
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        
        
        /// <summary>
        /// метод для открытия окна из проекта с обычным калькулятором 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openCalculator(object sender, RoutedEventArgs e)
        {
            var simpleCalculator = new ProstoyCalculator.MainWindow();
            Hide();
            simpleCalculator.ShowDialog();
            Show();
        }

        /// <summary>
        /// метод для открытия окна из проекта с парсящим калькулятором 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openParseCalc(object sender, RoutedEventArgs e)
        {
            var PCWindow = new Calculator.MainWindow();
            Hide();
            PCWindow.ShowDialog();
            Show();
        }

        /// <summary>
        /// метод для открытия окна из проекта с Пятнашками
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openPyatnashki(object sender, RoutedEventArgs e)
        {
            var PyatnashkiWindow = new Pyatnashki.MainWindow();
            Hide();
            PyatnashkiWindow.ShowDialog();
            Show();

        }

        /// <summary>
        /// метод для открытия окна из проекта с Судоку
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openSudoku(object sender, RoutedEventArgs e)
        {
            Sudoku.MainWindow sudokuWindow = new Sudoku.MainWindow();
            Hide();
            sudokuWindow.ShowDialog();
            Show();
        }
    }
}
