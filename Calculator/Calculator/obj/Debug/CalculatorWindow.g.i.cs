﻿#pragma checksum "..\..\CalculatorWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "DD2E9E41DBC50B3198159B564B8C14A0BAE5868A"
//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

using Calculator;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Calculator {
    
    
    /// <summary>
    /// MainWindow
    /// </summary>
    public partial class MainWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 13 "..\..\CalculatorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button b1;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\CalculatorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button b2;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\CalculatorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button b3;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\CalculatorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button b4;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\CalculatorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button b5;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\CalculatorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button b6;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\CalculatorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button b7;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\CalculatorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button b8;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\CalculatorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button b9;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\CalculatorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button plus;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\CalculatorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button b0;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\CalculatorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button minus;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\CalculatorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button div;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\CalculatorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button _is;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\CalculatorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox screen;
        
        #line default
        #line hidden
        
        
        #line 82 "..\..\CalculatorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button clear;
        
        #line default
        #line hidden
        
        
        #line 92 "..\..\CalculatorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button step;
        
        #line default
        #line hidden
        
        
        #line 93 "..\..\CalculatorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button sqrt;
        
        #line default
        #line hidden
        
        
        #line 103 "..\..\CalculatorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button point;
        
        #line default
        #line hidden
        
        
        #line 113 "..\..\CalculatorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button umn;
        
        #line default
        #line hidden
        
        
        #line 114 "..\..\CalculatorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button umn_Copy;
        
        #line default
        #line hidden
        
        
        #line 115 "..\..\CalculatorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button clear_Copy;
        
        #line default
        #line hidden
        
        
        #line 125 "..\..\CalculatorWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button minus_Copy;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Calculator;component/calculatorwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\CalculatorWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.b1 = ((System.Windows.Controls.Button)(target));
            
            #line 13 "..\..\CalculatorWindow.xaml"
            this.b1.Click += new System.Windows.RoutedEventHandler(this.pushButton);
            
            #line default
            #line hidden
            return;
            case 2:
            this.b2 = ((System.Windows.Controls.Button)(target));
            
            #line 23 "..\..\CalculatorWindow.xaml"
            this.b2.Click += new System.Windows.RoutedEventHandler(this.pushButton);
            
            #line default
            #line hidden
            return;
            case 3:
            this.b3 = ((System.Windows.Controls.Button)(target));
            
            #line 24 "..\..\CalculatorWindow.xaml"
            this.b3.Click += new System.Windows.RoutedEventHandler(this.pushButton);
            
            #line default
            #line hidden
            return;
            case 4:
            this.b4 = ((System.Windows.Controls.Button)(target));
            
            #line 25 "..\..\CalculatorWindow.xaml"
            this.b4.Click += new System.Windows.RoutedEventHandler(this.pushButton);
            
            #line default
            #line hidden
            return;
            case 5:
            this.b5 = ((System.Windows.Controls.Button)(target));
            
            #line 35 "..\..\CalculatorWindow.xaml"
            this.b5.Click += new System.Windows.RoutedEventHandler(this.pushButton);
            
            #line default
            #line hidden
            return;
            case 6:
            this.b6 = ((System.Windows.Controls.Button)(target));
            
            #line 36 "..\..\CalculatorWindow.xaml"
            this.b6.Click += new System.Windows.RoutedEventHandler(this.pushButton);
            
            #line default
            #line hidden
            return;
            case 7:
            this.b7 = ((System.Windows.Controls.Button)(target));
            
            #line 37 "..\..\CalculatorWindow.xaml"
            this.b7.Click += new System.Windows.RoutedEventHandler(this.pushButton);
            
            #line default
            #line hidden
            return;
            case 8:
            this.b8 = ((System.Windows.Controls.Button)(target));
            
            #line 47 "..\..\CalculatorWindow.xaml"
            this.b8.Click += new System.Windows.RoutedEventHandler(this.pushButton);
            
            #line default
            #line hidden
            return;
            case 9:
            this.b9 = ((System.Windows.Controls.Button)(target));
            
            #line 48 "..\..\CalculatorWindow.xaml"
            this.b9.Click += new System.Windows.RoutedEventHandler(this.pushButton);
            
            #line default
            #line hidden
            return;
            case 10:
            this.plus = ((System.Windows.Controls.Button)(target));
            
            #line 49 "..\..\CalculatorWindow.xaml"
            this.plus.Click += new System.Windows.RoutedEventHandler(this.pushButton);
            
            #line default
            #line hidden
            return;
            case 11:
            this.b0 = ((System.Windows.Controls.Button)(target));
            
            #line 59 "..\..\CalculatorWindow.xaml"
            this.b0.Click += new System.Windows.RoutedEventHandler(this.pushButton);
            
            #line default
            #line hidden
            return;
            case 12:
            this.minus = ((System.Windows.Controls.Button)(target));
            
            #line 60 "..\..\CalculatorWindow.xaml"
            this.minus.Click += new System.Windows.RoutedEventHandler(this.pushButton);
            
            #line default
            #line hidden
            return;
            case 13:
            this.div = ((System.Windows.Controls.Button)(target));
            
            #line 61 "..\..\CalculatorWindow.xaml"
            this.div.Click += new System.Windows.RoutedEventHandler(this.pushButton);
            
            #line default
            #line hidden
            return;
            case 14:
            this._is = ((System.Windows.Controls.Button)(target));
            
            #line 71 "..\..\CalculatorWindow.xaml"
            this._is.Click += new System.Windows.RoutedEventHandler(this.doClick);
            
            #line default
            #line hidden
            return;
            case 15:
            this.screen = ((System.Windows.Controls.TextBox)(target));
            return;
            case 16:
            this.clear = ((System.Windows.Controls.Button)(target));
            
            #line 82 "..\..\CalculatorWindow.xaml"
            this.clear.Click += new System.Windows.RoutedEventHandler(this.clearClick);
            
            #line default
            #line hidden
            return;
            case 17:
            this.step = ((System.Windows.Controls.Button)(target));
            
            #line 92 "..\..\CalculatorWindow.xaml"
            this.step.Click += new System.Windows.RoutedEventHandler(this.pushButton);
            
            #line default
            #line hidden
            return;
            case 18:
            this.sqrt = ((System.Windows.Controls.Button)(target));
            
            #line 93 "..\..\CalculatorWindow.xaml"
            this.sqrt.Click += new System.Windows.RoutedEventHandler(this.pushButton);
            
            #line default
            #line hidden
            return;
            case 19:
            this.point = ((System.Windows.Controls.Button)(target));
            
            #line 103 "..\..\CalculatorWindow.xaml"
            this.point.Click += new System.Windows.RoutedEventHandler(this.pushButton);
            
            #line default
            #line hidden
            return;
            case 20:
            this.umn = ((System.Windows.Controls.Button)(target));
            return;
            case 21:
            this.umn_Copy = ((System.Windows.Controls.Button)(target));
            
            #line 114 "..\..\CalculatorWindow.xaml"
            this.umn_Copy.Click += new System.Windows.RoutedEventHandler(this.pushButton);
            
            #line default
            #line hidden
            return;
            case 22:
            this.clear_Copy = ((System.Windows.Controls.Button)(target));
            
            #line 115 "..\..\CalculatorWindow.xaml"
            this.clear_Copy.Click += new System.Windows.RoutedEventHandler(this.clearOneClick);
            
            #line default
            #line hidden
            return;
            case 23:
            this.minus_Copy = ((System.Windows.Controls.Button)(target));
            
            #line 125 "..\..\CalculatorWindow.xaml"
            this.minus_Copy.Click += new System.Windows.RoutedEventHandler(this.pushButton);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

