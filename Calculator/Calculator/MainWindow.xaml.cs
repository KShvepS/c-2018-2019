﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Calculator
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            screen.IsReadOnly = true;
        }
        //массивы приоритетности действий 
        List<int> second = new List<int>();
        List<int> first = new List<int>();
        int l, r; //левая и правая граница
        /// <summary>
        /// Нажатие на кнопку (кроме равно)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pushButton(object sender, RoutedEventArgs e)
        {
            string s = (sender as Button).Content.ToString();
            char symb = s[0];
            if(Char.IsDigit(symb))//если вводимый символ - число
            {
                if (screen.Text != "0")
                {
                    screen.Text = screen.Text + s;
                }
                else
                {
                    screen.Text = s;
                }
            }
            else//если знак действия
            {
                if (s == "-numb")//для отрицательного числа
                {
                    if (screen.Text.Length == 0)//если минус будет первый символ
                    {
                        screen.Text = screen.Text + "-";
                    }
                    else if (screen.Text.Length >= 2)
                    {
                        if (!Char.IsDigit(screen.Text[screen.Text.Length - 1]) && //если пред символ
                            !screen.Text[screen.Text.Length - 1].Equals(",") &&//знак действия
                            Char.IsDigit(screen.Text[screen.Text.Length - 2]))//перед которым стоит число
                        {
                            screen.Text = screen.Text + "-";
                        }
                    }
                }
                else if (screen.Text.Length>0)//если было что-то введено
                {
                    char symb_before = screen.Text[screen.Text.Length - 1];//предыдущий символ
                    if (Char.IsDigit(symb_before))//число
                    {
                        if(symb == ',')//если нынешний символ запятая 
                        {
                            if (canPoint())//вызываем метод для проверки на добавление запятой
                            {
                                screen.Text = screen.Text + s;//тогда можем добавить знак запятой
                            }
                        }
                        else
                        {
                            screen.Text = screen.Text + s;//тогда можем добавить знак действия
                        }
                        
                    }
                }
            }
        }
        /// <summary>
        /// Очищение поля ввода полностью
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void clearClick(object sender, RoutedEventArgs e)
        {
            screen.Text = "";
        }
        /// <summary>
        /// Наажатие на знак "равно"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void doClick(object sender, RoutedEventArgs e)
        {
            if (screen.Text.Length>0)
            {
                char end = screen.Text[screen.Text.Length - 1];//смотрим последний символ

                if (!Char.IsDigit(end))//удаляем его, если это не знак действия
                {
                    screen.Text = screen.Text.Substring(0, screen.Text.Length - 1);
                }

                String lineScr = screen.Text;
                for (int i = 0; i < lineScr.Length; i++)
                {
                    if (lineScr[i].ToString().Equals("*") || lineScr[i].ToString().Equals("/") ||
                         lineScr[i].ToString().Equals("√") || lineScr[i].ToString().Equals("^"))
                    {
                        first.Add(i);
                    }
                    else if (lineScr[i].ToString().Equals("+") || lineScr[i].ToString().Equals("-"))
                    {
                        if (lineScr[i].ToString().Equals("-"))//исключаем случай с отрицательным числом
                        {
                            if (i != 0)//если символ не нулевой
                            {
                                if (i - 1 >= 0 && Char.IsDigit(lineScr[i - 1]))//если предыдущий симво число
                                {
                                    second.Add(i);
                                }
                            }
                        }
                        else
                        {
                            second.Add(i);
                        }

                    }
                }

                int min;// переменная для наим индекса
                double answ;

                while (first.Count != 0)//ищем наименьший индекс
                {
                    min = first.Min();
                    answ = getAnswer(min, lineScr);//начинаем решать выбранное по индексу действие
                    first.Remove(min);//удаление использованного действия
                    lineScr = changeValStr(min, answ, lineScr);//меняем индексы в массиве
                }

                while (second.Count != 0)
                {
                    min = second.Min();
                    answ = getAnswer(min, lineScr);//начинаем решать выбранное по индексу действие
                    second.Remove(min);//удаление использованного действия
                    lineScr = changeValStr(min, answ, lineScr);//меняем индексы в массиве
                }

                screen.Text = lineScr;
            }
        }
        /// <summary>
        /// метод для изменения строки после какого-то очередного выполнения действия
        ///  и для изменения индексов в знаков в массиве
        /// </summary>
        /// <param name="index"></param>
        /// <param name="val"></param>
        /// <param name="mainStr"></param>
        /// <returns></returns>
        private string changeValStr(int index, double val, String mainStr)
        {
            //меняем значения индексов в массиве
            for(int i = 0; i < first.Count;i++)
            {
                if(first[i]> index)
                {
                    first[i] = first[i] - (r - l + 1) + val.ToString().Length; // разница при сдвиге
                }
            }
            for (int i = 0; i < second.Count; i++)
            {
                if (second[i] > index)
                {
                    second[i] = second[i] - (r - l + 1) + val.ToString().Length; // разница при сдвиге
                }
            }
            return mainStr.Replace(mainStr.Substring(l,r-l+1), val.ToString());//меняем строку
        }
        /// <summary>
        /// считаем числа
        /// </summary>
        /// <param name="index"></param>
        /// <param name="mainStr"></param>
        /// <returns></returns>
        private double getAnswer(int index, string mainStr)
        {
            String left, right;
            left = right = "";
            int ileft, iright; // счётчики для выделения левой и правой части решаемого примера от знака действия
            ileft = index-1;
            iright = index+1;
            while (ileft >= 0)
            {

                if (ileft - 2 >= 0)
                {
                    if (Char.IsDigit(mainStr[ileft]) && !Char.IsDigit(mainStr[ileft - 1])&& 
                        Char.IsDigit(mainStr[ileft - 2]) && !mainStr[ileft - 1].Equals(','))
                    {
                        left += mainStr[ileft];
                        ileft--;
                        break;
                    }
                }
                else if (ileft-1>=0)
                {
                    if (!Char.IsDigit(mainStr[ileft]) && !mainStr[ileft].Equals(',') && Char.IsDigit(mainStr[ileft - 1]))
                    {
                        left += mainStr[ileft];
                        ileft--;
                        break;
                    }
                }
               
               
                left += mainStr[ileft];
                ileft--;
            }
            while (iright < mainStr.Length)
            {
                if(iright+1< mainStr.Length)
                {
                    if (Char.IsDigit(mainStr[iright]) && !Char.IsDigit(mainStr[iright + 1])
                        && !mainStr[iright + 1].Equals(','))//если последовательность = "цифра+знак(не запятая)"
                    {
                        right += mainStr[iright];
                        iright++;
                        break;
                    }
                }
                right += mainStr[iright];
                iright++;
            }
            left = ReverseString(left);
            char sign = mainStr[index];
            double answ = 0; // переменная, отвечающая за ответ в расчете
            switch(sign)// выбираем действие между найденными числами
            {
                case '+':
                    answ = Convert.ToDouble(left) + Convert.ToDouble(right);
                    break;
                case '-':
                    answ = Convert.ToDouble(left) - Convert.ToDouble(right);
                    break;
                case '*':
                    answ = Convert.ToDouble(left) * Convert.ToDouble(right);
                    break;
                case '/':
                    answ = Convert.ToDouble(left) / Convert.ToDouble(right);
                    break;
                case '^':
                    answ = Math.Pow(Convert.ToDouble(left), Convert.ToDouble(right));
                    break;
                case '√':
                    answ = Math.Pow(Convert.ToDouble(right), 1.0 / (Convert.ToDouble(left)));
                    break;
            }
            Console.WriteLine(" left = {0},right = {1}, answ = {2}", left, right, answ);
            l = ileft + 1;
            r = iright - 1;
            Console.WriteLine(" l = {0},r = {1}", l,r);
            return answ;
        }
        /// <summary>
        ///  левую часть надо перевернуть в обратную
        /// сторону из-за метода её считования
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string ReverseString(string s)
        {
            char[] arr = s.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }
        /// <summary>
        /// CE
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void clearOneClick(object sender, RoutedEventArgs e)
        {
            if(screen.Text.Length>0)
            {
                screen.Text = screen.Text.Substring(0, screen.Text.Length - 1);
            }
        }
        /// <summary>
        /// проверяем, можно ли ставить запятую 
        /// </summary>
        /// <returns></returns>
        private bool canPoint()
        {
            int i = screen.Text.Length-1;
            bool znak, zp;//наличие знака действие или запятой
            znak = zp = false;
            while(i >-1 && znak == false)
            {
                char symb = screen.Text[i];
                if (!Char.IsDigit(symb)) 
                {
                    if(symb != ',')
                    {
                        znak = true;
                    }
                    else
                    {
                        zp = true;
                    }
                }
               i--;
            }
            if(zp == false)//если не было запятой 
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
    }
}
