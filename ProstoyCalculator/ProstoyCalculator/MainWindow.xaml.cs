﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProstoyCalculator
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        String left = "";
        String right = "";
        String znak = "";
        /// <summary>
        /// Запись числа на экран и в переменные left & right
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void take_Numbers(object sender, RoutedEventArgs e)
        {
            string s = (sender as Button).Content.ToString();
            if (screen.Text != "0" &&( right != "" ||(left != "" && znak == "") || 
                (left != "" && znak != "" && screen.Text.IndexOf(',') == screen.Text.Length-1)))//чтоб нельзя было
                //ввести 02 и чтоб при каждом нажатии знака
                //дествия и последуещего нажатия цифры очищался экран
            {
                screen.Text = screen.Text + s;
            }
            else
            {
                screen.Text = s;
            }
            if (znak == "")//заполняем левую и правую часть в зависимости от ввода и наличия знака действия
                //(при первом вводе знак всегда пуст, поэтому сначала присваиваем левую часть, а потом 
                //всегда правую)
            {
                left = screen.Text;
            }
            else//
            {
                right = screen.Text;
            }
        }
        /// <summary>
        /// Стереть символ c экрана
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void clear_CE(object sender, RoutedEventArgs e)
        {
            if(screen.Text.Length>0)//если есть что стирать-стираем
            {
                screen.Text = screen.Text.Substring(0, screen.Text.Length - 1);
                left = screen.Text;
            }
        }
        /// <summary>
        /// Стереть все с экрана
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void clear_C(object sender, RoutedEventArgs e)
        {
            screen.Text = left = right = znak = ""; 
        }
        /// <summary>
        /// +-(число)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void push_PlusMinus(object sender, RoutedEventArgs e)
        {
            if (screen.Text != "")
            {
                left = Convert.ToString(-(Convert.ToDouble(left)));
                screen.Text = left;
            }
        }
        /// <summary>
        /// Нажатие на точку
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void push_Point(object sender, RoutedEventArgs e)
        {
            if ((screen.Text).IndexOf(",") == -1 && screen.Text != "")
            {
                screen.Text = screen.Text + (sender as Button).Content.ToString();
                if(right == "")//чтоб после первого действия при добавлении к новой левой части ,
               //после нажатия еще одного знака - правильно работала программа
                {
                    znak = "";
                } 
            }
        }
        /// <summary>
        /// Нажатие на кнопку с действием
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void push_znak(object sender, RoutedEventArgs e)
        {
            if(screen.Text.IndexOf(',') == screen.Text.Length-1 && screen.Text.Length !=0)//если забыли поставить цифру 
                //после запятой-считаем ее за ноль
            {
                if(right != "")
                {
                    right += ",0";
                    screen.Text = right;
                }
                else
                {
                    left += ",0";
                    screen.Text = left;
                }
            }

            if (right != "")//если есть что считать(и левая, и правая часть)
            {
                switch (znak)//всегда очищаем right в конце для правильности ввода
                {
                    case "+":
                        left = (Convert.ToDouble(left) + Convert.ToDouble(right)).ToString();
                        screen.Text = left;
                        right = "";
                        break;
                    case "-":
                        left = (Convert.ToDouble(left) - Convert.ToDouble(right)).ToString();
                        screen.Text = left;
                        right = "";
                        break;
                    case "*":
                        left = (Convert.ToDouble(left) * Convert.ToDouble(right)).ToString();
                        screen.Text = left;
                        right = "";
                        break;
                    case "/":
                        left = (Convert.ToDouble(left) / Convert.ToDouble(right)).ToString();
                        screen.Text = left;
                        right = "";
                        break;
                    case "^":
                        left = Math.Pow(Convert.ToDouble(left), Convert.ToDouble(right)).ToString();
                        screen.Text = left;
                        right = "";
                        break;
                    case "sqrt":
                        left = Math.Pow(Convert.ToDouble(left), 1.0 / (Convert.ToDouble(right))).ToString();
                        screen.Text = left;
                        right = "";
                        break;
                    case "=":
                        screen.Text = left;
                        right = "";
                        break;
                }
            }
            
            if(left != "")//если есть первое число - запоминаем знак
            {
                znak = (sender as Button).Content.ToString();
            }
           
        }

        /// <summary>
        /// Нажатие на кнопку с процентом
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void clicked_Procent(object sender, RoutedEventArgs e)//процент
        {
            if (screen.Text != "")
            {
                left = (Convert.ToDouble(left) / 100.0).ToString();
                screen.Text = left;
            }
        }
        
    }
}
