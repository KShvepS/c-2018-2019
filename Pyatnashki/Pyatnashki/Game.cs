﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pyatnashki
{
    class Game
    {
        int size;
        int[,] map;
        int space_x, space_y;
        static Random rand = new Random();

        public Game (int sizet)
        {
            size = sizet;
            map = new int[size, size];
        }

        public void start()
        {
            for(int i = 0; i < size; i++)
            {
                for(int j = 0; j<size; j++)
                {
                    map[i, j] = coords_to_position(i, j)+1;
                }
            }
            space_x = size - 1;
            space_y = size - 1;
            map[space_x, space_y] = 0;
        }

        public void shift(int position)//обмен значений клетками
        {
            int x, y;
            position_to_coords(position, out x, out y);
            if(Math.Abs(space_x - x) + Math.Abs(space_y - y) != 1)//не дает кнопкам на расстоянии 
                //больеш чем 1 шага перепрыгивать на место "пустой"
            {
                return;
            }
            map[space_x, space_y] = map[x, y];
            map[x, y] = 0;
            space_x = x;
            space_y = y;
        }

        public void shift_random()
        {
            int x = space_x;
            int y = space_y;
            int a = rand.Next(0, 4);//максимум 4 хода
            switch (a)
            {
                case 0: x--;break;
                case 1: x++;break;
                case 2: y--;break;
                case 3: y++;break;
            }
            shift(coords_to_position(x,y));
        }

        public bool is_win()
        {
            if(!(space_x == size-1 && space_y == size-1))
            {
                return false;
            }
            for(int i = 0; i < size; i++)
            {
                for(int j = 0; j < size; j++)
                {
                    if(!(i == space_x && j == space_y))
                    {
                       if (!(map[i, j] == coords_to_position(i, j) + 1))
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        public int get_number(int x, int y)
        {
            return map[x, y];//возвращаем значение числа клетки 
        }

        private int coords_to_position(int x, int y)
        {
            if (x < 0) x = 0;
            if (x > size - 1) x = size - 1;
            if (y < 0) y = 0;
            if (y > size - 1) y = size - 1;

            return x * size + y;
        }

        private void position_to_coords(int position, out int x, out int y)
        {
            if (position < 0) position = 0;
            if (position > size * size - 1) position = size * size - 1;
            y = position % size;
            x = position / size;
        }
    }
}
