﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using System.Windows.Interop;
namespace Pyatnashki
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static Button[,] field; // поле кнопок для игры
        public static ImageSource[] image_filed; //массив для картинок
        int sizegrid;
        int imgnow, lastimg;
        Game game;

        public MainWindow()
        {
            InitializeComponent();
            build_field();// строим поле по умолчанию и мнимое поле, перемешиваем значения
            random_image();//выбираем картинку
            take_img();//помещаем на поле
            refresh();//указываем значения на кнопках
        }

        private void refresh()
        {
            for(int i = 0; i < sizegrid;i++)
            {
                for (int j = 0; j < sizegrid; j++)
                {
                    field[i, j].Content = game.get_number(i, j).ToString();
                    if ((field[i, j].Content).Equals("0"))//не показывает последнее число
                    {
                        field[i, j].Visibility = Visibility.Hidden;
                    }
                    else
                    {
                        field[i, j].Visibility = Visibility.Visible;
                        field[i, j].Background = new ImageBrush(image_filed[(game.get_number(i, j)) - 1]);
                    }
                }
            }
        }

        private void new_game()//новая игра
        {
            game = new Game(sizegrid);//создаем мнимое поле
            game.start();//заполняем поле
        }
        
        private void build_field() // перестройка поля кнопок, перемешиваем цифры
        {
            game_field.RowDefinitions.Clear();
            game_field.ColumnDefinitions.Clear();
            game_field.Children.Clear();

            if (sizegrid == 0)
            {
                sizegrid =  3;
            }

            new_game();//делаем новую игру
     
            //перемешиваем поле методом, при котором можно будет пройти игру
            for (int i = 0; i < 100; i++)
            {
                game.shift_random();
            }

            field = new Button[sizegrid, sizegrid];
            //строим поле 
            int h = Convert.ToInt32(game_field.Height / sizegrid);
            int w = Convert.ToInt32(game_field.Width / sizegrid);
            ColumnDefinition[] col = new ColumnDefinition[sizegrid];
            RowDefinition[] row = new RowDefinition[sizegrid];
            //game_field.ShowGridLines = true;
            for (int i = 0; i < sizegrid; i++)
            {
                row[i] = new RowDefinition();
                game_field.RowDefinitions.Add(row[i]);
                col[i] = new ColumnDefinition();
                game_field.ColumnDefinitions.Add(col[i]);
                
            }
           
            for (int i = 0; i < sizegrid; i++)
            {
                for (int j = 0; j < sizegrid; j++)
                {
                    field[i, j] = new Button();
                    field[i, j].Style = (Style)this.FindResource("ButtonFiled");
                    field[i, j].Tag = ( i * sizegrid + j+1).ToString();
                    field[i, j].Height = h;
                    field[i, j].Width = w;
                    this.game_field.Children.Add(field[i, j]);
                    Grid.SetColumn(field[i, j], j);
                    Grid.SetRow(field[i, j], i);
                    //field[i, j].Margin = new Thickness(5, 5, 0, 0);
                    field[i, j].Click += button_Click;
                }
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            game.shift(Convert.ToInt32((sender as Button).Tag.ToString())-1);
            refresh();
            if(game.is_win())
            {
                Win win_window = new Win();
                this.Hide();
                win_window.ShowDialog();
                this.Show();
                build_field();// строим поле по умолчанию и мнимое поле, перемешиваем значения
                refresh();
            }
            
        }

        private void play_Selected(object sender, RoutedEventArgs e) // перезапускает поле заново
        {
            build_field();// строим поле по умолчанию и мнимое поле, перемешиваем значения
            refresh();//указываем значения на кнопках
        }

        public void take_img()
        {
            Bitmap fone = Properties.Resources.cat;
            switch (imgnow)
            {
                case 1:
                    fone = Properties.Resources.cat;
                    break;
                case 2:
                    fone = Properties.Resources.dog;
                    break;
                case 3:
                    fone = Properties.Resources.shiba;
                    break;
            }

            image_filed = new ImageSource[sizegrid * sizegrid];

            using (var src = new Bitmap(fone))//режем выбранную картинку
            {
                using (var dst = new Bitmap(Properties.Resources.cat.Width/sizegrid, Properties.Resources.cat.Height/ sizegrid, src.PixelFormat))
                using (var gfx = Graphics.FromImage(dst))
                    for (int y = 0; y < sizegrid; y++)
                    {
                        for (int x = 0; x < sizegrid; x++)
                        {
                            var rec = new System.Drawing.Rectangle(x * Convert.ToInt32(game_field.Width / sizegrid), y * Convert.ToInt32(game_field.Height / sizegrid), Convert.ToInt32(game_field.Width/sizegrid), Convert.ToInt32(game_field.Width / sizegrid));
                            gfx.DrawImage(src, 0, 0, rec, GraphicsUnit.Pixel);
                            var handle1 = dst.GetHbitmap();
                            image_filed[y * sizegrid + x] =  Imaging.CreateBitmapSourceFromHBitmap(handle1, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                            //!!!!!!!field[y, x].Background = new ImageBrush(Imaging.CreateBitmapSourceFromHBitmap(handle1, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions()));
                        }
                    }
            }
            
        }

        private void imgg_Click(object sender, RoutedEventArgs e) // смена картинки
        {
            lastimg = imgnow;
            random_image();
            take_img();
            refresh();
        }

        public void random_image()
        {
            Random rnd = new Random();

            while (lastimg == imgnow) // выбираем новую картинку
            {
                imgnow = rnd.Next(1,4);

            }
        }

        public void resize_Click(object sender, RoutedEventArgs e)//при нажатии на изменение размера поля
        {
            sizegrid = Convert.ToInt32((sender as MenuItem).Tag.ToString());
            build_field();
            take_img();//помещаем на поле
            refresh();
        }
    }
}
